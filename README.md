# photoguin
**Repository that I am using to learn rust**  

Goal of this repository is to help me managed orphaned (RAW / unprocessed) photography files on my computer.  After years of photography all of these orphaned files add up to a lot of space.  (this tool could be a simple python script but wanted to learn rust)

## Help Menu
![Help Menu](/assets/help.jpg)  

# Personal
## General Workflow for Tool
1. Go through deleting unsalvagable JPEGs
2. Run photguin to clean out all the orphaned raw files
3. Import RAW files and edit them in editor while cleaning more files I don't like
4. Rerun photguin taking care of remainder of the orphaned files

# Todo
- [ ] Learn how to write docstrings and make my code actually not hot garbage in terms of documentation
- [ ] Learn how to setup up cross compilation for linux in repo
- [ ] Learn how to put release on GitLab so I don't have to compile for my Windows/Linux seperate places
- [ ] Double check if I need the few clone statements I have done
- [ ] Clean up organziation. My functions are in a confusing heap of spaghetti across files.  Felt like function colocation was a lot different then python and need to learn to place them better
- [x] Struct consuming Some/None instead of ImageType
- [ ] Learn better best practices for clap argumenet parsing what I did is a bit clunky