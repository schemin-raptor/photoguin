use std::path::PathBuf;

use glob::glob_with;
use glob::MatchOptions;


/// Finds files with extension in the current working directory
pub fn find_extension(file_extension_glob: &str) -> Vec<PathBuf> {
    let options = MatchOptions {
        case_sensitive: false,
        require_literal_separator: true,
        require_literal_leading_dot: false,
    };
    let mut paths = Vec::new();
    for entry in glob_with(file_extension_glob, options).expect("Improper glob path") {
        match entry {
            Ok(path) => paths.push(path),
            Err(_e) => {
                let message = format!(
                    "Failed to find any matching files for the following pattern {}",
                    file_extension_glob
                );
                println!("{}", message);
                std::process::exit(1);
            }
        };
    }
    paths.to_owned()
}

#[cfg(test)]
mod tests {
    use super::find_extension;
    use std;
    use std::path::Path;

    #[test]
    fn test_find_extension() {
        let path = Path::new("tests/test_files");
        std::env::set_current_dir(path).unwrap();
        let glob_path = "*.jpg";
        let paths_vecs = find_extension(glob_path);
        assert_eq!(paths_vecs, vec![Path::new("test_1.jpg"), Path::new("test_2.jpg")])
    }
}
