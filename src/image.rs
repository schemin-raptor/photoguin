use simple_error::SimpleError;
use std::{collections::HashMap, path::PathBuf};
use std::collections::hash_map::Entry;

/// ImageType enum where JPEG represents post processed values and RAW represents RAW camera values
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ImageType {
    RAW,
    JPEG,
}

/// Image struct
#[derive(Debug, Clone, PartialEq)]
pub struct Image {
    path: PathBuf,
    raw_image: Option<()>,
    jpeg_image: Option<()>,
}

impl Image {
    fn new(path: PathBuf, image_type: ImageType) -> Self {
        let image_instance = match image_type {
            ImageType::RAW => Self {
                path: path.clone(),
                raw_image: Some(()),
                jpeg_image: None,
            },
            ImageType::JPEG => Self {
                path: path.clone(),
                raw_image: None,
                jpeg_image: Some(()),
            },
        };
        return image_instance;
    }

    fn update(&mut self, image_type: ImageType) -> Result<(), SimpleError> {
        match image_type {
            ImageType::JPEG => match self.jpeg_image {
                Some(()) => Err(SimpleError::new("Duplicate Entry")),
                None => {
                    self.jpeg_image = Some(());
                    Ok(())
                }
            },
            ImageType::RAW => match self.raw_image {
                Some(()) => Err(SimpleError::new("Duplicate Entry")),
                None => {
                    self.raw_image = Some(());
                    Ok(())
                }
            },
        }
    }

    /// Checks whether a image is complete has both RAW and JPEG
    fn is_complete(&self) -> bool {
        let image_tuple = (&self.raw_image, &self.jpeg_image);
        match image_tuple {
            (Some(()), Some(())) => true,
            _ => false,
        }
    }
    /// Checks whether a RAW is orphaned
    fn is_raw_orphaned(&self) -> Result<bool, SimpleError> {
        let image_tuple = (&self.raw_image, &self.jpeg_image);
        match image_tuple {
            (Some(()), None) => Ok(true),
            (Some(()), Some(())) => Ok(false),
            _ => Err(SimpleError::new(
                "Comparison does not make sense as image has no raw",
            )),
        }
    }

    // Static Methods

    /// Static Method
    /// Returns orphaned images that are found in a hashmap
    pub fn find_orphaned(images: &HashMap<String, Image>) -> Vec<String> {
        // Notes for Tim to look in the future at
        // (1st filter) filtering down keys that responde with no error and are true (in that they are orphaned)
        // (1st Map) cloning key to put into vector
        // (Collect) collecting vector, having to specify type with turbo operator
        let orphaned_images = images
            .iter()
            .filter(|&(_, value)| Image::is_raw_orphaned(&value) == Ok(true))
            .map(|(key, _)| key.clone())
            .collect::<Vec<String>>();
        orphaned_images
    }

    /// Returns a hashmap of type <String, Image>
    pub fn create_hashmap () -> HashMap<String,Image> {
        HashMap::new()
    }

    /// Static Method
    /// Add image to images struct hashmap
    pub fn add_image(
        images: &mut HashMap<String, Image>,
        image_base_name: String,
        image_base_path: PathBuf,
        image_type: ImageType,
    ) -> Result<(), SimpleError> {
        let mut result = Ok(()); 
        let _ = images
            .entry(image_base_name)
            .and_modify(|image| result = image.update(image_type))
            .or_insert(Image::new(image_base_path, image_type));

        result
    }
}
#[cfg(test)]
mod tests {
    use simple_error::SimpleError;

    use super::Image;
    use super::ImageType;
    use std::collections::HashMap;
    use std::path::PathBuf;

    struct Setup {
        solo_jpg: Image,
        solo_raw: Image,
        complete: Image,
    }
    impl Setup {
        fn new() -> Self {
            Self {
                solo_jpg: Image {
                    path: PathBuf::from(r"test"),
                    raw_image: None,
                    jpeg_image: Some(()),
                },
                solo_raw: Image {
                    path: PathBuf::from("test"),
                    raw_image: Some(()),
                    jpeg_image: None,
                },
                complete: Image {
                    path: PathBuf::from("test"),
                    raw_image: Some(()),
                    jpeg_image: Some(()),
                },
            }
        }
        fn create_hashmap() -> HashMap<String, Image> {
            let images = Self::new();
            HashMap::from([
                (String::from("solo_jpg"), images.solo_jpg.clone()),
                (String::from("solo_raw"), images.solo_raw.clone()),
                (String::from("complete"), images.complete.clone()),
            ])
        }
    }

    #[test]
    fn test_new_raw() {
        let generated_image = Image::new(PathBuf::from(r"test"), ImageType::RAW);
        assert_eq!(
            generated_image,
            Image {
                path: PathBuf::from(r"test"),
                raw_image: Some(()),
                jpeg_image: None
            }
        )
    }

    #[test]
    fn test_new_jpeg() {
        let generated_image = Image::new(PathBuf::from(r"test"), ImageType::JPEG);
        assert_eq!(
            generated_image,
            Image {
                path: PathBuf::from(r"test"),
                raw_image: None,
                jpeg_image: Some(())
            }
        )
    }
    #[test]
    fn test_update_1() {
        let mut solo_jpg = Setup::new().solo_jpg.to_owned();
        let complete = Setup::new().complete.to_owned();
        let result = solo_jpg.update(ImageType::RAW);
        assert_eq!(result, Ok(()));
        assert_eq!(solo_jpg, complete);
    }
    #[test]
    fn test_update_2() {
        let mut solo_jpg = Setup::new().solo_jpg.to_owned();
        let result = solo_jpg.update(ImageType::JPEG);
        assert_eq!(result, Err(SimpleError::new("Duplicate Entry")));
    }
    #[test]
    fn test_is_complete() {
        let image = Setup::new().complete.to_owned();
        assert!(Image::is_complete(&image))
    }
    #[test]
    fn test_is_complete_2() {
        let image = Setup::new().solo_jpg.to_owned();
        assert!(!Image::is_complete(&image))
    }
    #[test]
    fn test_is_orphaned() {
        let image = Setup::new().complete.to_owned();
        assert_eq!(Image::is_raw_orphaned(&image), Ok(false))
    }
    #[test]
    fn test_is_orphaned_2() {
        let image = Setup::new().solo_raw.to_owned();
        assert_eq!(Image::is_raw_orphaned(&image), Ok(true))
    }
    #[test]
    fn test_is_orphaned_3() {
        let image = Setup::new().solo_jpg.to_owned();
        assert_eq!(
            Image::is_raw_orphaned(&image),
            Err(SimpleError::new(
                "Comparison does not make sense as image has no raw"
            ))
        )
    }
    #[test]
    fn test_find_orphaned_1() {
        let hashmap = Setup::create_hashmap();
        let orphaned = Image::find_orphaned(&hashmap);
        assert_eq!(orphaned, Vec::from(["solo_raw"]))
    }
    #[test]
    fn test_add_image_1() {
        let mut solo_jpg = Setup::new().solo_jpg;
        let _ = solo_jpg.update(ImageType::RAW).unwrap();
        let mut hashmap = Setup::create_hashmap();
        let a = Image::add_image(&mut hashmap, String::from("solo_jpg"), PathBuf::from("test"), ImageType::RAW);
        println!("{:?}", a);
        assert_eq!(hashmap["solo_jpg"], solo_jpg)
    }
    #[test]
    fn test_add_image_2() {
        let mut hashmap = Setup::create_hashmap();
        let result = Image::add_image(&mut hashmap, String::from("solo_jpg"), PathBuf::from("test"), ImageType::JPEG);
        assert_eq!(result, Err(SimpleError::new("Duplicate Entry")))
    }
}
