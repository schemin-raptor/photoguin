use std::{os::raw, path::Path, path::PathBuf, fs};

use clap::{Arg, Parser};

use crate::{locating, image::ImageType};

#[derive(Parser, Debug)]
#[command(author = "Tim Kogucki", version = "0.1.0", about= "A CLI tool to help manage orphaned image files and clear up disk space", long_about = None)]
pub struct Args {
    /// Relative path to directory to search (leave empty for currenty directory)
    #[arg(short, long)]
    dir: Option<String>,

    /// File extension for processed image format (omit the "." in the file extension)
    #[arg(short = 'p', long, default_value = "jpg")]
    processed_format: String,

    /// File extension for pre-processed image format (omit the "." in the file extension)
    #[arg(short = 'r', long, default_value = "arw")]
    raw_format: String,

    /// List all orphaned files
    #[arg(short, long)]
    pub list_orphans: bool,

    /// Delete all orphaned files
    #[arg(short = 'x', long)]
    pub delete_orphans: bool,

    /// Calculate storaged saved by removing all orphaned files
    #[arg(short = 'c', long)]
    pub calculate_size_of_orphaned: bool,
}

impl Args {
    /// Parses arguments and creates some helper structures
    pub fn arg_parse() -> (Self, GlobHelpers) {
        let args = Args::parse();
        let dir_path = match &args.dir {
            Some(string) => std::env::current_dir().unwrap().join(&string).to_owned(),
            None => std::env::current_dir().unwrap().to_owned(),
        };
        let glob_helper = args.make_glob_helper(dir_path);
        // println!("{:#?}", args);
        // println!("{:#?}", glob_helpers);
        // println!("{}", glob_helpers.dir_path.display());
        // println!("{:#?}", files);

        (args, glob_helper)
    }

    /// Returns inputed processed format and raw format into a glob pattern
    fn make_glob_helper(&self, dir_path: PathBuf) -> GlobHelpers {
        let processed_glob = "*.".to_string() + &self.processed_format;
        let raw_glob = "*.".to_string() + &self.raw_format;
        GlobHelpers::new(processed_glob, raw_glob, dir_path.to_owned())
    }

}

#[derive(Debug, PartialEq)]
pub struct GlobHelpers {
    glob_processed: String,
    glob_raw: String,
    pub dir_path: PathBuf,
}
impl GlobHelpers {
    pub fn new(glob_processed: String, glob_raw: String, dir_path: PathBuf) -> Self {
        GlobHelpers {
            glob_processed,
            glob_raw,
            dir_path,
        }
    }
    
    /// Finds raw and jpeg files, returns both vectors in a tuple
    /// Returns:
    ///     (raw_files, jpeg_files)
    pub fn find_files(&self) -> (Vec<PathBuf>, Vec<PathBuf>) {
        // TODO: This only allows relative paths at the moment
        let prev_dir = std::env::current_dir().unwrap();
        std::env::set_current_dir(&self.dir_path).unwrap();
        let raw_files = locating::find_extension(&self.glob_raw);
        let jpeg_files = locating::find_extension(&self.glob_processed);
        std::env::set_current_dir(&prev_dir).unwrap();
        (raw_files, jpeg_files)
    }
    pub fn construct_file_path(&self, file_name: &String, image_type: ImageType) -> PathBuf {
        let extension: &str = match image_type {
            ImageType::JPEG => self.glob_processed.split(".").collect::<Vec<_>>()[1],
            ImageType::RAW=> self.glob_raw.split(".").collect::<Vec<_>>()[1],
        };
        let file_name = format!("{}.{}", file_name, extension);
       self.dir_path.clone().join(file_name)

    }
}

pub fn evaluate_arg_actions(args: &Args, glob_helpers: &GlobHelpers ,orphaned: &Vec<String>) {
    if orphaned.len() == 0{
        println!("There are no orphaned files");
        return
    }
    if args.list_orphans {
        println!("Orphaned files in {}", glob_helpers.dir_path.display());
        for orphan in orphaned{
            let output = format!("\t {}", orphan);
            println!("{}", output);
          }
    }
    let mut file_paths;
    // This if statements lets us be a little faster with preallocating Vec on the heap
    // Otherwise I initialize vec with zero capacity as we won't use it
    // Goal here was to save the n * k times needing of this path generation in function calls
    if args.calculate_size_of_orphaned || args.delete_orphans {
        file_paths = Vec::with_capacity(orphaned.len());
        for orphan in orphaned{
            file_paths.push(glob_helpers.construct_file_path(orphan, ImageType::RAW));
        } 
    } else {
            // Here preallocating with capacity zero as we don't intend to use it
            file_paths = Vec::with_capacity(0);
    }
    if args.calculate_size_of_orphaned {
        let dir_path = glob_helpers.dir_path.clone();
        println!("Calculating size of orphaned files in {}", &dir_path.display());
        let mut sum_file_size: u64 = 0;
        for file_path in &file_paths{
            let file_size = file_path.metadata()
                                        .expect("Unable to get file metadata, check if files are open")
                                        .len();
            let file_size  = file_size;
            sum_file_size += file_size;
            // println!("{:?}", )
        }
        println!("\t Total orphaned file size is: {:.4} GB", sum_file_size as f64 / 1000000000.0);
    }
    if args.delete_orphans {
        let dir_path = glob_helpers.dir_path.clone();
        println!("Deleting orphaned files in {}", &dir_path.display());
        let mut sum_file_size: u64 = 0;
        for file_path in &file_paths{
            let file_size = file_path.metadata()
                                        .expect("Unable to get file metadata, check if files are open")
                                        .len();
            let file_size  = file_size;
            sum_file_size += file_size;
            let file_removal = fs::remove_file(file_path);
            match file_removal {
                Ok(()) => println!("\t\tSuccessfully remove file at: {}", file_path.display()),
                Err(e) => {
                    println!("\t\tCould not remove file at {}", file_path.display());
                    println!("\t\t\tError caused by {}", e);
                }
            }
        }
        println!("\t Total orphaned file size deleted: {:.4} GB", sum_file_size as f64 / 1000000000.0);

    }
}