mod commands;
mod image;
mod locating;

use std::collections::HashMap;
use std::path::PathBuf;

use crate::commands::evaluate_arg_actions;
use crate::image::Image;
use crate::image::ImageType;

fn main() {
    let (args, glob_helpers) = commands::Args::arg_parse();
    let (raw_files, processed_files) = glob_helpers.find_files();
    let mut image_map = Image::create_hashmap();
    // println!("{:#?}", raw_files);
    // println!("{:#?}", processed_files);
    hashmap_update(&mut image_map, &processed_files, ImageType::JPEG);
    hashmap_update(&mut image_map, &raw_files, ImageType::RAW);
    // println!("{:#?}", image_map)
    let orphaned = Image::find_orphaned(&image_map);
    evaluate_arg_actions(&args, &glob_helpers, &orphaned);
}


pub fn hashmap_update(image_map: &mut HashMap<String, Image>, files: &Vec<PathBuf>, image_type: ImageType) {
    for file in files{
        let base_name = String::from(file.file_stem().unwrap().to_str().unwrap());
        let base_path = file.parent().unwrap().to_path_buf();
        let result = Image::add_image(
            image_map,
            base_name.clone(),
            base_path.clone(),
            image_type,
        );
        match result {
            Ok(()) => {},
            Err(err) => {
                println!("{}", err);
                println!("Error: duplicate values with base {} in {}", base_name, base_path.display());
                std::process::exit(1)
            },
        }
    }

}